import { AppRegistry } from 'react-native';

import App from './src/app';
import { name } from './app.json';

AppRegistry.registerComponent(name, () => App);
